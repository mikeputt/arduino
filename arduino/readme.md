#Spring Boot RESTful Web Service Example

##1. About

This app uses Spring Boot to create a microservice with a few simple API endpoints that return JSON data.
It implements an H2 embedded database to store the temperature objects.

An Arduino connected to a temperature sensor sends temperature and humidity data to the API making a POST request every 10 minutes.
This only works on the local WiFi as the app must be running on a predefined static IP.

##2. Getting Started

The app runs at localhost on port 8081.
The landing page should give a "Hello World!" page when the app is up and running.
http://localhost:8081/

The [AccuWeather API](http://apidev.accuweather.com/developers/) can be used to fetch some initial data by making a POST request to following URI:
http://localhost:8081/api/accuweather

This will give you some data with temperatures for the last 24 hours.

Note: The AccuWeather API only allows 50 calls per day.

##3. API Endpoints

The API endpoints available are as follows:

* http://localhost:8081/api/temperature - GET and POST
    * POST requires a temp and humidity value 
* http://localhost:8081/api/temperature/{id} - GET, PUT, and DELETE
    * PUT requires a temp and humidity value
* http://localhost:8081/api/accuweather - GET and POST

Note: The date field is created at the time the data is sent and not used as an input parameter.

##4. Views

The app uses embedded Tomcat with a JSP template to create a web page and display the data with HTML, CSS, and JavaScript.
An ajax GET request is made to the temperature API using JavaScript and the data displayed with the Google Charts library.

To view the data go to the URI: http://localhost:8081/view

![image](src/main/resources/static/img/view.jpg)

##5. Useful Tools

###a. Postman

Postman is a useful tool that can be used to test the endpoints of the API are giving the expected responses.
It can also be used to test invalid inputs and expected error response messages.

![image](src/main/resources/static/img/postman.jpg)

###b. Swagger

A basic implementation of Swagger has been set up but not fully configured.
It can be used to test endpoints of the API and gives you an overview of available all endpoints and responses.

Swagger can be accessed here:
http://localhost:8081/swagger-ui.html

![image](src/main/resources/static/img/swagger.jpg)

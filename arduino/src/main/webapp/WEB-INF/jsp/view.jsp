<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="language" content="english" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <link rel="stylesheet" type="text/css" src="/css/styles.css">
    <script type="text/javascript" src="/js/scripts.js"></script>
</head>
<body>
<style>html body {
    font-size: 16px;
    background-color: #000;
    color: #fff;
}</style>
    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12">
                <h1>Data from Arduino</h1>
                <h2 class="mt-4">Temperature</h2>
                <div id="dashboard_div">
                    <div id="filter_div"></div>
                    <div id="curve_chart"></div>
                </div>
                <h2 class="mt-4">Humidity</h2>
                <div id="dashboard_div">
                    <div id="hum_chart"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
(jQuery)(document).ready(function($) {
    google.charts.load( 'current', { 'packages':['corechart'] } );
    google.charts.setOnLoadCallback( drawChart );
    google.charts.setOnLoadCallback( drawChart2 );
});

var date_sort_asc = function (a, b) {
  return new Date(a.date) - new Date(b.date);
};

function drawChart() {
    $.getJSON("http://localhost:8081/api/temperature", function(result){
        var width = (jQuery)( '#curve_chart' ).width()-30;
        var data = new google.visualization.DataTable();
        data.addColumn( 'datetime', 'Date' );
        data.addColumn( 'number', 'Temp' );
        var length = result.length;
        var id, temp, humidity, date;
        sortedResult = result.sort(date_sort_asc);

        $.each(sortedResult, function(key, value) {
            if ( value.temp != "" && value.date != "" ) {
                id = value.id
                temp = value.temp;
                humidity = value.humidity;
                date = value.date;
                data.addRow( [ new Date(date), temp ] );
                console.log(date);
            }
        })

        var options = {
            width: width,
            height: 500,
            chartArea: {
                width: width-60,
                height: 400,
                right: 10
            },
            hAxis: {
                format: 'dd/MM ha',
                viewWindowMode: 'explicit',
                viewWindow: {
                    min: new Date(new Date().getTime() - 12*60*60*1000),
                    max: new Date(new Date().getTime())
                },
                textStyle: {
                    color: '#fff'
                }
            },
            vAxis: {
                minValue: 0,
                textStyle: {
                    color: '#fff'
                },
                format: '# °C'
            },
            explorer: {
                axis: 'horizontal',
                keepInBounds: false
            },
            legend: {
                position: 'none'
            },
            backgroundColor: '#000'
        };

        var chart = new google.visualization.LineChart(
            document.getElementById( 'curve_chart' )
        );

        chart.draw( data, options );
    });
}

function drawChart2() {
    $.getJSON("http://localhost:8081/api/temperature", function(result){
        var width = (jQuery)( '#curve_chart' ).width()-30;
        var data = new google.visualization.DataTable();
        data.addColumn( 'datetime', 'Date' );
        data.addColumn( 'number', 'Temp' );
        var length = result.length;
        var id, temp, humidity, date;
        sortedResult = result.sort(date_sort_asc);

        $.each(result, function(key, value) {
            if ( value.temp != "" && value.date != "" ) {
                id = value.id
                temp = value.temp;
                humidity = value.humidity / 100;
                date = value.date;
                data.addRow( [ new Date(date), humidity ] );
            }
        })

        var options = {
            width: width,
            height: 500,
            chartArea: {
                width: width-60,
                height: 400,
                right: 10
            },
            hAxis: {
                format: 'dd/MM ha',
                viewWindowMode: 'explicit',
                viewWindow: {
                    min: new Date(new Date().getTime() - 12*60*60*1000),
                    max: new Date(new Date().getTime())
                },
                textStyle: {
                    color: '#fff'
                }
            },
            vAxis: {
                minValue: 0,
                textStyle: {
                    color: '#fff'
                },
                format: '#%'
            },
            explorer: {
                axis: 'horizontal',
                keepInBounds: false
            },
            legend: {
                position: 'none'
            },
            backgroundColor: '#000'
        };

        var chart = new google.visualization.LineChart(
            document.getElementById( 'hum_chart' )
        );

        chart.draw( data, options );
    });
}
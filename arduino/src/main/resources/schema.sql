DROP TABLE IF EXISTS temperature_entity;

CREATE TABLE IF NOT EXISTS temperature_entity (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    temp DOUBLE NOT NULL,
    humidity DOUBLE NOT NULL,
    datetime TIMESTAMP
);

/*INSERT INTO temperature_entity (temp, humidity, datetime)
    VALUES ('25.00', '70.00', '2020-02-09 10:30:00');*/
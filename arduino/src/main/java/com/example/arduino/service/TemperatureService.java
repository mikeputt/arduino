package com.example.arduino.service;

import com.example.arduino.exceptions.NotFoundException;
import com.example.arduino.controller.api.model.TemperatureDto;
import com.example.arduino.repository.TemperatureRepository;
import com.example.arduino.repository.model.TemperatureEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemperatureService {

    //@Autowired
    //private TemperatureH2RepositoryImpl temperatureRepository;

    @Autowired
    private TemperatureRepository temperatureRepository;

    private TemperatureDto temperatureDto;

    public TemperatureService() {
    }

    public List<TemperatureDto> getTemperature() {
        TemperatureMapper temperatureMapper = new TemperatureMapper();
        List<TemperatureEntity> temperatureEntity = temperatureRepository.getAll();
        return temperatureMapper.mapEntityToDto(temperatureEntity);
    }

    public TemperatureDto createTemperature(TemperatureDto temperatureDto) {
        TemperatureMapper temperatureMapper = new TemperatureMapper();
        TemperatureEntity temperatureEntity = temperatureMapper.mapDtoToEntity(temperatureDto);
        temperatureRepository.create(temperatureEntity);
        return temperatureMapper.mapEntityToDto(temperatureEntity);
    }

    public TemperatureDto getTemperatureById(int id) {
        TemperatureMapper temperatureMapper = new TemperatureMapper();
        TemperatureEntity temperatureEntity = temperatureRepository.get(id);

        if (temperatureEntity == null) {
            throw new NotFoundException();
        }

        return temperatureMapper.mapEntityToDto(temperatureEntity);
    }

    public TemperatureDto updateTemperatureById(int id, TemperatureDto temperatureDto) {
        TemperatureMapper temperatureMapper = new TemperatureMapper();
        TemperatureEntity temperatureEntity = temperatureMapper.mapDtoToEntity(temperatureDto);
        temperatureRepository.update(id, temperatureEntity);
        return temperatureMapper.mapEntityToDto(temperatureEntity);
    }

    public TemperatureDto deleteTemperatureById(int id) {
        TemperatureMapper temperatureMapper = new TemperatureMapper();
        TemperatureEntity temperatureEntity = temperatureRepository.get(id);
        temperatureRepository.delete(id);
        return temperatureMapper.mapEntityToDto(temperatureEntity);
    }

}
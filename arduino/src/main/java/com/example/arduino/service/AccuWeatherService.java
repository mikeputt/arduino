package com.example.arduino.service;

import com.example.arduino.controller.api.model.AccuWeatherDto;
import com.example.arduino.controller.api.model.TemperatureDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class AccuWeatherService {

    @Autowired
    TemperatureService temperatureService;

    public List<AccuWeatherDto> get() throws ParseException, JsonProcessingException {
        String accuWeather = "http://dataservice.accuweather.com/currentconditions/v1/250435/historical/24?apikey=G4oxsWULQnBIA4LcjgW7hAh0a8gSGs4A";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseBody = restTemplate.getForEntity(accuWeather, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nodes = mapper.readTree(responseBody.getBody());
        Iterator<JsonNode> nodesArray = nodes.elements();
        List<AccuWeatherDto> accuWeatherDtoList = new ArrayList<>();

        while (nodesArray.hasNext()) {
            JsonNode node = nodesArray.next();
            double temp = node.path("Temperature").path("Metric").path("Value").doubleValue();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            Date datetime = formatter.parse(node.path("LocalObservationDateTime").textValue());
            AccuWeatherDto accuWeatherDto = new AccuWeatherDto(temp, datetime);
            accuWeatherDtoList.add(accuWeatherDto);
        }

        return accuWeatherDtoList;
    }

    public List<TemperatureDto> create() throws ParseException, JsonProcessingException {
        String accuWeather = "http://dataservice.accuweather.com/currentconditions/v1/250435/historical/24?apikey=G4oxsWULQnBIA4LcjgW7hAh0a8gSGs4A";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseBody = restTemplate.getForEntity(accuWeather, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nodes = mapper.readTree(responseBody.getBody());
        Iterator<JsonNode> nodesArray = nodes.elements();
        List<TemperatureDto> temperatureDtoList = new ArrayList<>();

        while (nodesArray.hasNext()) {
            JsonNode node = nodesArray.next();
            double temp = node.path("Temperature").path("Metric").path("Value").doubleValue();
            double humidity = 0.00;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            Date datetime = formatter.parse(node.path("LocalObservationDateTime").textValue());
            TemperatureDto temperatureDto = new TemperatureDto(temp, humidity, datetime);
            temperatureDto = temperatureService.createTemperature(temperatureDto);
            temperatureDtoList.add(temperatureDto);
        }

        return temperatureDtoList;
    }

}
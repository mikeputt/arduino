package com.example.arduino.service;

import com.example.arduino.controller.api.model.TemperatureDto;
import com.example.arduino.repository.model.TemperatureEntity;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class TemperatureMapper {

    public TemperatureEntity mapDtoToEntity(TemperatureDto temperatureDto) {
        TemperatureEntity temperatureEntity = new TemperatureEntity();
        BeanUtils.copyProperties(temperatureDto, temperatureEntity, "id");
        return temperatureEntity;
    }

    public TemperatureDto mapEntityToDto(TemperatureEntity temperatureEntity) {
        TemperatureDto temperatureDto = new TemperatureDto();
        BeanUtils.copyProperties(temperatureEntity, temperatureDto);
        return temperatureDto;
    }

    public List<TemperatureEntity> mapDtoToEntity(List<TemperatureDto> temperatureDto) {
        List<TemperatureEntity> temperatureEntity = new ArrayList<TemperatureEntity>();

        for (int i = 0; i < temperatureDto.size(); i++) {
            TemperatureDto dto = temperatureDto.get(i);
            TemperatureEntity entity = new TemperatureEntity();
            BeanUtils.copyProperties(dto, entity);
            temperatureEntity.add(i, entity);
        }

        return temperatureEntity;
    }

    public List<TemperatureDto> mapEntityToDto(List<TemperatureEntity> temperatureEntity) {
        List<TemperatureDto> temperatureDto = new ArrayList<TemperatureDto>();

        for (int i = 0; i < temperatureEntity.size(); i++) {
            TemperatureEntity entity = temperatureEntity.get(i);
            TemperatureDto dto = new TemperatureDto();
            BeanUtils.copyProperties(entity, dto);
            temperatureDto.add(i, dto);
        }

        return temperatureDto;
    }

}

package com.example.arduino.repository;

import com.example.arduino.repository.model.TemperatureEntity;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class TemperatureRepositoryImpl implements TemperatureRepository {

    private Map<Integer, TemperatureEntity> temperatureEntities;
    private int index;

    public TemperatureRepositoryImpl() {
        temperatureEntities = new HashMap<>();
        index = 1;

        Date datetime = new Date( System.currentTimeMillis() );
        TemperatureEntity a = new TemperatureEntity(1, 25.00, 70.00, datetime);
        temperatureEntities.put(1, a);
    }

    @Override
    public TemperatureEntity get(int id) {
        return temperatureEntities.get(id);
    }

    @Override
    public List<TemperatureEntity> getAll() {
        return new ArrayList<>(temperatureEntities.values());
    }

    @Override
    public TemperatureEntity create(TemperatureEntity temperatureEntity) {
        temperatureEntity.setId(index);
        temperatureEntities.put(index, temperatureEntity);
        index++;
        return temperatureEntity;
    }

    @Override
    public TemperatureEntity update(int id, TemperatureEntity temperatureEntity) {
        temperatureEntities.put(id, temperatureEntity);
        return temperatureEntity;
    }

    @Override
    public TemperatureEntity delete(int id) {
        return temperatureEntities.remove(id);
    }

}
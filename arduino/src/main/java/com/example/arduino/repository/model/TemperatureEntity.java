package com.example.arduino.repository.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class TemperatureEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private double temp;
    private double humidity;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date datetime;

    public TemperatureEntity() {}

    public TemperatureEntity(int id, double temp, double humidity, Date datetime) {
        this.id = id;
        this.temp = temp;
        this.humidity = humidity;
        this.datetime = datetime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setDate(Date datetime) {
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public double getTemp() {
        return temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public Date getDate() {
        return datetime;
    }

}
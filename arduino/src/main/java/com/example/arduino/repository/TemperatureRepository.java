package com.example.arduino.repository;

import com.example.arduino.repository.model.TemperatureEntity;

import java.util.List;

public interface TemperatureRepository extends Repository<TemperatureEntity> {

    TemperatureEntity get(int id);

    List<TemperatureEntity> getAll();

    TemperatureEntity create(TemperatureEntity temperatureEntity);

    TemperatureEntity update(int id, TemperatureEntity temperatureEntity);

    TemperatureEntity delete(int id);

}
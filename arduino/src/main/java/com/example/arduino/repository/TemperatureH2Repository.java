package com.example.arduino.repository;

import com.example.arduino.repository.model.TemperatureEntity;
import org.springframework.data.repository.CrudRepository;

public interface TemperatureH2Repository extends CrudRepository<TemperatureEntity, Integer> {}
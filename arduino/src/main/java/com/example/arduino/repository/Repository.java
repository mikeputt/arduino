package com.example.arduino.repository;

import java.util.List;

public interface Repository<T> {

    T get(int id);

    List<T> getAll();

    T create(T t);

    T update(int id, T t);

    T delete(int id);

}
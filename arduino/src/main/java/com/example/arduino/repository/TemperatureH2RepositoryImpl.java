package com.example.arduino.repository;

import com.example.arduino.exceptions.NotFoundException;
import com.example.arduino.repository.model.TemperatureEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Profile("h2")
@Primary
@Repository
public class TemperatureH2RepositoryImpl implements TemperatureRepository {

    @Autowired
    @Lazy
    private TemperatureH2Repository temperatureH2Repository;

    public TemperatureH2RepositoryImpl() {}

    @Override
    public TemperatureEntity get(int id) {
        Optional<TemperatureEntity> temperatureEntityOp = temperatureH2Repository.findById(id);
        TemperatureEntity temperatureEntity = new TemperatureEntity();

        if (!temperatureEntityOp.isPresent()) {
            throw new NotFoundException();
        }

        temperatureEntityOp.ifPresent(temperatureEntity1 -> {
            BeanUtils.copyProperties(temperatureEntity1, temperatureEntity);
        });

        return temperatureEntity;
    }

    @Override
    public List<TemperatureEntity> getAll() {
        Iterable<TemperatureEntity> temperatureEntityItList = temperatureH2Repository.findAll();
        List<TemperatureEntity> temperatureEntityList = (List<TemperatureEntity>) temperatureEntityItList;
        return temperatureEntityList;
    }

    @Override
    public TemperatureEntity create(TemperatureEntity temperatureEntity) {
        temperatureH2Repository.save(temperatureEntity);
        return temperatureEntity;
    }

    @Override
    public TemperatureEntity update(int id, TemperatureEntity temperatureEntity) {
        temperatureH2Repository.save(temperatureEntity);
        return temperatureEntity;
    }

    @Override
    public TemperatureEntity delete(int id) {
        Optional<TemperatureEntity> temperatureEntityOp = temperatureH2Repository.findById(id);
        TemperatureEntity temperatureEntity = new TemperatureEntity();

        if (!temperatureEntityOp.isPresent()) {
            throw new NotFoundException();
        }

        temperatureEntityOp.ifPresent(temperatureEntity1 -> {
            BeanUtils.copyProperties(temperatureEntity1, temperatureEntity);
        });

        temperatureH2Repository.deleteById(temperatureEntity.getId());
        return temperatureEntity;
    }

}

package com.example.arduino.controller.api.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class AccuWeatherDto {

    private double temp;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date datetime;

    public AccuWeatherDto() {}

    public AccuWeatherDto(double temp, Date datetime) {
        this.temp = temp;
        this.datetime = datetime;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setDate(Date datetime) {
        this.datetime = datetime;
    }

    public double getTemp() {
        return temp;
    }

    public Date getDate() {
        return datetime;
    }

}
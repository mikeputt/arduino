package com.example.arduino.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TemperatureDto {
    @ApiParam(hidden=true)
    private int id;

    @JsonProperty("temp")
    private double temp;

    @JsonProperty("humidity")
    private double humidity;

    @ApiParam(hidden=true)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date datetime;

    public TemperatureDto() {}

    public TemperatureDto(int id, double temp, double humidity, Date datetime) {
        this.id = id;
        this.temp = temp;
        this.humidity = humidity;
        this.datetime = datetime;
    }

    public TemperatureDto(double temp, double humidity, Date datetime) {
        this.temp = temp;
        this.humidity = humidity;
        this.datetime = datetime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setDate(Date datetime) {
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public double getTemp() {
        return temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public Date getDate() {
        return datetime;
    }

}
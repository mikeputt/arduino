package com.example.arduino.controller.api;

import com.example.arduino.controller.api.model.AccuWeatherDto;
import com.example.arduino.controller.api.model.TemperatureDto;
import com.example.arduino.exceptions.NotFoundException;
import com.example.arduino.service.AccuWeatherService;
import com.example.arduino.service.TemperatureService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path="/api")
public class TemperatureController {

    @Autowired
    private TemperatureService temperatureService;

    @Autowired
    private AccuWeatherService accuWeatherService;

    @RequestMapping(value="/temperature", method=RequestMethod.GET)
    public ResponseEntity<List<TemperatureDto>> getAll() {
        List<TemperatureDto> temperatureDto = temperatureService.getTemperature();
        return new ResponseEntity<>(temperatureDto, HttpStatus.OK);
    }

    @RequestMapping(value="/temperature", method=RequestMethod.POST)
    public ResponseEntity<TemperatureDto> create (
            TemperatureDto temperatureDto
    ) throws NumberFormatException {
        temperatureDto.setDate(new Date( System.currentTimeMillis() ));
        temperatureDto = temperatureService.createTemperature(temperatureDto);
        return new ResponseEntity<>(temperatureDto, HttpStatus.OK);
    }

    @RequestMapping(value="/temperature/{id}", method=RequestMethod.GET)
    public ResponseEntity<TemperatureDto> get(@PathVariable int id) throws NotFoundException {
        TemperatureDto temperatureDto = temperatureService.getTemperatureById(id);
        return new ResponseEntity<>(temperatureDto, HttpStatus.OK);
    }

    @RequestMapping(value="/temperature/{id}", method=RequestMethod.PUT)
    public ResponseEntity<TemperatureDto> update(
            @PathVariable int id, TemperatureDto temperatureDto
    ) throws NumberFormatException {
        temperatureDto.setId(id);
        temperatureDto.setDate(new Date( System.currentTimeMillis() ));
        temperatureService.getTemperatureById(id);
        temperatureService.updateTemperatureById(id, temperatureDto);
        return new ResponseEntity<>(temperatureDto, HttpStatus.OK);
    }

    @RequestMapping(value="/temperature/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> delete(
            @PathVariable int id
        ) throws NotFoundException {
        temperatureService.deleteTemperatureById(id);
        String message = "Temperature with id=" + id + " was removed";
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    //@RequestMapping(value="/temperature/recent", method=RequestMethod.GET)
    //public ResponseEntity<List<TemperatureDto>> getRecent(@RequestParam(defaultValue = "10") int number) {
    //    List<TemperatureDto> temperatureDto = temperatureService.getTemperature(); // limit to number requested
    //    return new ResponseEntity<>(temperatureDto, HttpStatus.OK);
    //}

    @RequestMapping(value="/accuweather", method=RequestMethod.GET)
    public ResponseEntity<List<AccuWeatherDto>> getAccuWeather() throws ParseException, JsonProcessingException {
        List<AccuWeatherDto> accuWeatherDtoList = accuWeatherService.get();
        return new ResponseEntity<>(accuWeatherDtoList, HttpStatus.OK);
    }

    @RequestMapping(value="/accuweather", method=RequestMethod.POST)
    public ResponseEntity<List<TemperatureDto>> createAccuWeather() throws ParseException, JsonProcessingException {
        List<TemperatureDto> temperatureDtoList = accuWeatherService.create();
        return new ResponseEntity<>(temperatureDtoList, HttpStatus.OK);
    }

    @RequestMapping(value="/test", method=RequestMethod.GET)
    public String getTest() {
        return "Hello World!";
    }

    @RequestMapping(value="/test2", method=RequestMethod.DELETE)
    public String deleteTest() {
        return "Bye Bye World!";
    }

}
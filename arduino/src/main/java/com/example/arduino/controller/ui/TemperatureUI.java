package com.example.arduino.controller.ui;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class TemperatureUI {

    @GetMapping({"/", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping("/view")
    public String view() {
        return "view";
    }

}

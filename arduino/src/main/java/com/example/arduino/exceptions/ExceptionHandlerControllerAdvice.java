package com.example.arduino.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(IllegalArgumentException.class)
	public final ResponseEntity<Object> handleNotFoundException(IllegalArgumentException e, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse("No temperature found with that id", HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	// not used
	@ExceptionHandler(NotANumberException.class)
	public final ResponseEntity<ExceptionResponse> handleNotANumberException(NotANumberException e, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse(e.getMessage(), HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public final ResponseEntity<ExceptionResponse> handleTypeMismatchException(MethodArgumentTypeMismatchException e, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse("Input is not valid", HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(final MissingServletRequestParameterException e, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		String message = "Missing request paramater: " + e.getParameterName();
		ExceptionResponse error = new ExceptionResponse(message, HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(GeneralException.class)
	public final ResponseEntity<ExceptionResponse> handleGeneralException(GeneralException e, WebRequest request) {
		ExceptionResponse error = new ExceptionResponse("An error occurred", HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
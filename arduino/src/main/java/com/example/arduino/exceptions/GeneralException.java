package com.example.arduino.exceptions;

public class GeneralException extends Exception {

    public GeneralException() {
        super("An error occurred");
    }

    public GeneralException(final String message) {
        super(message);
    }

}

package com.example.arduino.exceptions;

public class NotFoundException extends IllegalArgumentException {

    public NotFoundException() { // or use the default message
        super("No temperature was found with that id");
    }

    public NotFoundException(String message){ // you can pass in your own message
        super(message);
    }

}
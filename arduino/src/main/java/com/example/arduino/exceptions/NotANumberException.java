package com.example.arduino.exceptions;

public class NotANumberException extends NumberFormatException {

    public NotANumberException() { // or use the default message
        super("Number is not valid");
    }

    public NotANumberException(String message){ // you can pass in your own message
        super(message);
    }
}

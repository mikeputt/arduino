package com.example.arduino.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.List;

public class ExceptionResponse {

	private String message;
	private int status;
	private List<String> details;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private LocalDateTime timestamp;

	public ExceptionResponse(String message, List<String> details) {
		super();
		this.message = message;
		this.details = details;
	}

	public ExceptionResponse(String message, int status) {
		super();
		this.message = message;
		this.timestamp = LocalDateTime.now();
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void getMessage(LocalDateTime datetime) {
		this.timestamp = datetime;
	}

	public String getMessage() {
		return this.message;
	}

	public int getStatus() {
		return this.status;
	}

	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}
}
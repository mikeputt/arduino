package com.example.arduino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArduinoMain {

	public static void main(String[] args) {
		SpringApplication.run(ArduinoMain.class, args);
	}

}

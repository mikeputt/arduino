package com.example.arduino;

import com.example.arduino.controller.api.model.TemperatureDto;
import com.example.arduino.repository.TemperatureRepositoryImpl;
import com.example.arduino.repository.model.TemperatureEntity;
import com.example.arduino.service.TemperatureService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.IterableUtil.sizeOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ArduinoMainTests {

    @Autowired
    TemperatureService temperatureService;

    @Autowired
    TemperatureRepositoryImpl temperatureRepository;

    @Autowired
    MockMvc mockMvc;

    TemperatureDto temperatureDto;

    int id = 1;
    double temp = 25.00;
    double humidity = 70.00;
    Date datetime = new Date(System.currentTimeMillis());

    @Before
    public void setUp() {
        temperatureDto = new TemperatureDto();
        temperatureDto.setId(id);
        temperatureDto.setTemp(temp);
        temperatureDto.setHumidity(humidity);
        temperatureDto.setDate(datetime);
    }

    @Test
    public void whenCreateTemperature_thenReturnTemperatureEntity() throws Exception {
        // given
        TemperatureEntity temperatureEntity = new TemperatureEntity();

        // when
        try {
            temperatureService.createTemperature(temperatureDto);
            temperatureEntity = temperatureRepository.get(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // then
        assertThat(temperatureDto.getId())
                .isEqualTo(temperatureEntity.getId());

        mockMvc.perform(post("/api/temperature?temp="+temp+"&humidity="+humidity)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()
                );
    }

    @Test
    public void whenGetTemperatureById_thenReturnTemperature() throws Exception {

        temperatureService.createTemperature(temperatureDto);

        try {
            temperatureDto = temperatureService.getTemperatureById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        TemperatureEntity temperatureEntity = temperatureRepository.get(id);

        // then
        assertThat(temperatureDto.getId())
                .isEqualTo(temperatureEntity.getId());

        mockMvc.perform(get("/api/temperature/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()
            );
    }

    @Test
    public void whenDeleteTemperatureById_thenReturnTemperature() throws Exception {

        temperatureService.createTemperature(temperatureDto);
        TemperatureEntity temperatureEntity = temperatureRepository.get(id);

        mockMvc.perform(delete("/api/temperature/"+id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()
                );

        // then
        assertThat(temperatureDto.getId())
                .isEqualTo(temperatureEntity.getId());

        try {
            temperatureDto = temperatureService.deleteTemperatureById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void whenGetTemperature_thenReturnAllTemperatures() throws Exception {

        List<TemperatureDto> temperatureDtoList = new ArrayList<>();
        temperatureDtoList.add(temperatureDto);

        try {
            temperatureDtoList = temperatureService.getTemperature();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<TemperatureEntity> temperatureEntityList = temperatureRepository.getAll();

        // then
        for (int i = 1; i < sizeOf(temperatureDtoList); i++) {
            assertThat(temperatureDtoList.get(i).getId())
                    .isEqualTo(temperatureEntityList.get(i).getId());
        }

        mockMvc.perform(get("/api/temperature")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()
                );
    }

}
